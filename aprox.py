#!/usr/bin/env python3

'''
Cálculo del número óptimo de árboles.
'''

import sys

# Variables globales
base_trees = 0
fruit_per_tree = 0
reduction = 0

def compute_trees(trees):
    global base_trees, fruit_per_tree, reduction

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):
    global base_trees, fruit_per_tree, reduction

    productions = [(num_trees, compute_trees(num_trees)) for num_trees in range(min_trees, max_trees + 1)]
    return productions

def read_arguments():
    global base_trees, fruit_per_tree, reduction

    # Verificar que el número de argumentos sea correcto
    if len(sys.argv) != 6:
        print("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
        sys.exit(1)

    # Obtener argumentos de la línea de comandos y convertirlos a enteros
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min_trees = int(sys.argv[4])
        max_trees = int(sys.argv[5])
    except ValueError:
        print("All arguments must be integers")
        sys.exit(1)

    return base_trees, fruit_per_tree, reduction, min_trees, max_trees

def main():
    global base_trees, fruit_per_tree, reduction

    # Obtener los argumentos de la línea de comandos
    base_trees, fruit_per_tree, reduction, min_trees, max_trees = read_arguments()

    # Calcular todas las producciones
    productions = compute_all(min_trees, max_trees)

    # Encontrar la mejor producción y su correspondiente número de árboles
    best_num_trees, best_production = max(productions, key=lambda x: x[1])
    print(f"Best production: {best_production}, for {best_num_trees} trees")

if __name__ == '__main__':
    main()
